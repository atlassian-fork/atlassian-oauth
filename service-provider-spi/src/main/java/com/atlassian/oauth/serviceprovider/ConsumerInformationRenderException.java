package com.atlassian.oauth.serviceprovider;

/**
 * Exception that is thrown when the consumer information renderer fails to render consumer information for some reason.
 */
public class ConsumerInformationRenderException extends RuntimeException {
    public ConsumerInformationRenderException(String message, Throwable e) {
        super(message, e);
    }
}
