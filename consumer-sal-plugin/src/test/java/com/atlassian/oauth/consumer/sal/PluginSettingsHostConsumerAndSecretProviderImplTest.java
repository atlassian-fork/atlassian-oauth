package com.atlassian.oauth.consumer.sal;

import com.atlassian.oauth.Consumer.SignatureMethod;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.consumer.core.ConsumerServiceStore.ConsumerAndSecret;
import com.atlassian.oauth.consumer.core.HostConsumerAndSecretProvider;
import com.atlassian.oauth.util.RSAKeys;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;
import java.util.Map;
import java.util.Properties;

import static com.atlassian.oauth.consumer.sal.PluginSettingsHostConsumerAndSecretProviderImpl.HOST_SERVICENAME;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER;
import static com.atlassian.oauth.testing.TestData.KEYS;
import static com.google.common.collect.Maps.newHashMap;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PluginSettingsHostConsumerAndSecretProviderImplTest {
    @Mock
    ApplicationProperties applicationProperties;
    @Mock
    KeyPairFactory keyPairFactory;
    @Mock
    I18nResolver i18nResolver;
    Map<String, Object> settings;

    HostConsumerAndSecretProvider provider;

    @Before
    public void setUp() {
        PluginSettingsFactory pluginSettingsFactory = mock(PluginSettingsFactory.class);
        settings = newHashMap();
        when(pluginSettingsFactory.createGlobalSettings()).thenReturn(new MapBackedPluginSettings(settings));

        provider = new PluginSettingsHostConsumerAndSecretProviderImpl(applicationProperties, pluginSettingsFactory, keyPairFactory, i18nResolver);
    }

    @Test
    public void assertThatExistingHostConsumerAndSecretAreReturned() throws Exception {
        Properties props = new Properties();
        props.put("key", "Test:1234");
        props.put("name", "Test");
        props.put("signatureMethod", SignatureMethod.RSA_SHA1.name());
        props.put("publicKey", RSAKeys.toPemEncoding(KEYS.getPublic()));
        props.put("privateKey", RSAKeys.toPemEncoding(KEYS.getPrivate()));
        props.put("description", "A test consumer");
        props.put("callback", "http://consumer/callback");
        settings.put(ConsumerService.class.getName() + ":host." + HOST_SERVICENAME, props);

        ConsumerAndSecret cas = provider.get();

        assertNotNull("consumerAndSecret is null", cas);
        assertThat(cas.getConsumer().getKey(), is(equalTo("Test:1234")));
        assertThat(cas.getConsumer().getName(), is(equalTo("Test")));
        assertThat(cas.getConsumer().getPublicKey(), is(equalTo(KEYS.getPublic())));
        assertThat(cas.getConsumer().getDescription(), is(equalTo("A test consumer")));
        assertThat(cas.getConsumer().getCallback(), is(equalTo(URI.create("http://consumer/callback"))));
        assertThat(cas.getPrivateKey(), is(equalTo(KEYS.getPrivate())));
    }

    @Test
    public void assertThatNewHostConsumerAndSecretAreCreatedWhenItDoesNotExist() throws Exception {
        when(keyPairFactory.newKeyPair()).thenReturn(KEYS);
        when(applicationProperties.getDisplayName()).thenReturn("Test");

        ConsumerAndSecret cas = provider.get();

        assertNotNull("consumerAndSecret is null", cas);
        assertThat(cas.getConsumer().getKey(), startsWith("Test:"));
        assertThat(cas.getConsumer().getName(), is(equalTo("Test")));
        assertThat(cas.getConsumer().getPublicKey(), is(equalTo(KEYS.getPublic())));
        assertThat(cas.getPrivateKey(), is(equalTo(KEYS.getPrivate())));

        Properties props = (Properties) settings.get(ConsumerService.class.getName() + ":host." + HOST_SERVICENAME);
        assertNotNull("no consumerAndSecret properties in settings", props);
        assertThat(props, is(equalTo(new ConsumerProperties(cas).asProperties())));
    }

    @Test
    public void assertThatPutHostConsumerAndSecretAreStoredInSettings() {
        ConsumerAndSecret cas = new ConsumerAndSecret(HOST_SERVICENAME, RSA_CONSUMER, KEYS.getPrivate());

        provider.put(cas);

        Properties props = (Properties) settings.get(ConsumerService.class.getName() + ":host." + HOST_SERVICENAME);
        assertNotNull("no consumerAndSecret properties in settings", props);
        assertThat(props, is(equalTo(new ConsumerProperties(cas).asProperties())));
    }
}
