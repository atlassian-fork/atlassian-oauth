package com.atlassian.oauth.consumer.sal;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.Consumer.SignatureMethod;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.consumer.core.ConsumerServiceStore;
import com.atlassian.oauth.consumer.core.ConsumerServiceStore.ConsumerAndSecret;
import com.atlassian.oauth.util.RSAKeys;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Properties;

import static com.atlassian.oauth.testing.Matchers.mapWithKeys;
import static com.atlassian.oauth.testing.Matchers.withStringLength;
import static com.atlassian.oauth.testing.TestData.Consumers.HMAC_CONSUMER_WITH_LONG_KEY;
import static com.google.common.collect.Maps.newHashMap;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PluginSettingsConsumerServiceStoreTest {
    Map<String, Object> settings;

    ConsumerServiceStore store;

    @Before
    public void setUp() {
        settings = newHashMap();

        PluginSettingsFactory pluginSettingsFactory = mock(PluginSettingsFactory.class);
        when(pluginSettingsFactory.createGlobalSettings()).thenReturn(new MapBackedPluginSettings(settings));

        store = new PluginSettingsConsumerServiceStore(pluginSettingsFactory);
    }

    @Test
    public void assertThatGetConsumerReturnsExistingConsumerInfo() throws NoSuchAlgorithmException {
        KeyPair keyPair = RSAKeys.generateKeyPair();

        Properties props = new Properties();
        props.put("key", "Test:1234");
        props.put("name", "Test");
        props.put("signatureMethod", SignatureMethod.RSA_SHA1.name());
        props.put("publicKey", RSAKeys.toPemEncoding(keyPair.getPublic()));
        props.put("privateKey", RSAKeys.toPemEncoding(keyPair.getPrivate()));
        props.put("description", "A test consumer");
        props.put("callback", "http://consumer/callback");
        settings.put(ConsumerService.class.getName() + ".testservice", props);

        ConsumerAndSecret cas = store.get("testservice");

        assertNotNull("consumerAndSecret is null", cas);
        assertThat(cas.getConsumer().getKey(), is(equalTo("Test:1234")));
        assertThat(cas.getConsumer().getName(), is(equalTo("Test")));
        assertThat(cas.getConsumer().getPublicKey(), is(equalTo(keyPair.getPublic())));
        assertThat(cas.getConsumer().getDescription(), is(equalTo("A test consumer")));
        assertThat(cas.getConsumer().getCallback(), is(equalTo(URI.create("http://consumer/callback"))));
        assertThat(cas.getPrivateKey(), is(equalTo(keyPair.getPrivate())));
    }

    @Test
    public void assertThatGetConsumerByKeyReturnsExistingConsumerInfo() throws NoSuchAlgorithmException {
        KeyPair keyPair = RSAKeys.generateKeyPair();

        settings.put(ConsumerService.class.getName() + ".consumerService.Test:1234", "testservice");
        Properties props = new Properties();
        props.put("key", "Test:1234");
        props.put("name", "Test");
        props.put("signatureMethod", SignatureMethod.RSA_SHA1.name());
        props.put("publicKey", RSAKeys.toPemEncoding(keyPair.getPublic()));
        props.put("privateKey", RSAKeys.toPemEncoding(keyPair.getPrivate()));
        props.put("description", "A test consumer");
        props.put("callback", "http://consumer/callback");
        settings.put(ConsumerService.class.getName() + ".testservice", props);

        ConsumerAndSecret cas = store.getByKey("Test:1234");

        assertThat(cas.getConsumer().getKey(), is(equalTo("Test:1234")));
        assertThat(cas.getConsumer().getName(), is(equalTo("Test")));
        assertThat(cas.getConsumer().getPublicKey(), is(equalTo(keyPair.getPublic())));
        assertThat(cas.getConsumer().getDescription(), is(equalTo("A test consumer")));
        assertThat(cas.getConsumer().getCallback(), is(equalTo(URI.create("http://consumer/callback"))));
        assertThat(cas.getPrivateKey(), is(equalTo(keyPair.getPrivate())));
    }

    @Test
    public void assertThatAddConsumerAddsToSettingsWithPublicPrivateKeys() throws NoSuchAlgorithmException {
        KeyPair keyPair = RSAKeys.generateKeyPair();

        Consumer consumer = Consumer.key("Test:1234")
                .name("Test")
                .signatureMethod(SignatureMethod.RSA_SHA1)
                .publicKey(keyPair.getPublic())
                .description("A test consumer")
                .callback(URI.create("http://consumer/callback"))
                .build();
        ConsumerAndSecret cas = new ConsumerAndSecret("testservice", consumer, keyPair.getPrivate());

        store.put("testservice", cas);

        Properties props = new Properties();
        props.put("key", "Test:1234");
        props.put("name", "Test");
        props.put("signatureMethod", SignatureMethod.RSA_SHA1.name());
        props.put("publicKey", RSAKeys.toPemEncoding(keyPair.getPublic()));
        props.put("privateKey", RSAKeys.toPemEncoding(keyPair.getPrivate()));
        props.put("description", "A test consumer");
        props.put("callback", "http://consumer/callback");

        assertThat((String) settings.get(ConsumerService.class.getName() + ".consumerService.Test:1234"), is(equalTo("testservice")));
        assertThat((Properties) settings.get(ConsumerService.class.getName() + ".testservice"), is(equalTo(props)));
    }

    @Test
    public void assertThatAddConsumerAddsToSettingsWithSharedSecret() throws NoSuchAlgorithmException {
        Consumer consumer = Consumer.key("Test:1234")
                .name("Test")
                .signatureMethod(SignatureMethod.HMAC_SHA1)
                .description("A test consumer")
                .callback(URI.create("http://consumer/callback"))
                .build();
        ConsumerAndSecret cas = new ConsumerAndSecret("testservice", consumer, "s3kr3t");

        store.put("testservice", cas);

        Properties props = new Properties();
        props.put("key", "Test:1234");
        props.put("name", "Test");
        props.put("signatureMethod", SignatureMethod.HMAC_SHA1.name());
        props.put("sharedSecret", "s3kr3t");
        props.put("description", "A test consumer");
        props.put("callback", "http://consumer/callback");

        assertThat((String) settings.get(ConsumerService.class.getName() + ".consumerService.Test:1234"), is(equalTo("testservice")));
        assertThat((Properties) settings.get(ConsumerService.class.getName() + ".testservice"), is(equalTo(props)));
    }

    @Test
    public void assertThatRemoveByKeyNullsThePluginSetting() {
        settings.put(ConsumerService.class.getName() + ".serviceNames", "firstservice/testservice/lastservice");
        settings.put(ConsumerService.class.getName() + ".consumerService.Test:1234", "testservice");
        Properties props = new Properties();
        props.put("key", "Test:1234");
        settings.put(ConsumerService.class.getName() + ".testservice", props);

        store.removeByKey("Test:1234");

        assertTrue("mapping to service name was removed", !settings.containsKey(ConsumerService.class.getName() + ".consumerService.Test:1234"));
        assertTrue("consumer properties were removed", !settings.containsKey(ConsumerService.class.getName() + ".testservice"));
        assertThat((String) settings.get(ConsumerService.class.getName() + ".serviceNames"), not(containsString("testservice")));
    }

    @Test
    public void assertThatGetAllServiceProvidersReturnsEmptyIterableWhenServiceNamesEntryIsBlank() {
        settings.put(ConsumerService.class.getName() + ".serviceNames", "");
        assertThat(store.getAllServiceProviders(), is(Matchers.<Consumer>emptyIterable()));
    }

    @Test
    public void assertThatConsumerPropertyKeysAreLessThanOneHundredCharacters() {
        ConsumerAndSecret cas = new ConsumerAndSecret("testservice", HMAC_CONSUMER_WITH_LONG_KEY, "s3kr3t");
        store.put("testService", cas);

        assertThat(settings, mapWithKeys(withStringLength(lessThanOrEqualTo(100))));
    }
}
