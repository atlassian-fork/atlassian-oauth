package com.atlassian.oauth.event;

/**
 * This is an abstract base class for token removal events.
 */
public abstract class TokenRemovedEvent {
    private final String username;

    protected TokenRemovedEvent(String username) {
        this.username = username;
    }

    /**
     * Returns the name of the user the token belonged to.
     *
     * @return name of the user the token belonged to
     */
    public String getUsername() {
        return username;
    }
}
