package com.atlassian.oauth.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class CheckTest {
    @Test
    public void regularStringArentBlank() {
        assertEquals("test", Check.notBlank("test", "message"));
    }

    @Test(expected = NullPointerException.class)
    public void nullIsBlank() {
        Check.notBlank(null, "message");
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyStringsAreBlank() {
        Check.notBlank("", "message");
    }

    @Test(expected = IllegalArgumentException.class)
    public void whitespaceStringsAreBlank() {
        Check.notBlank(" ", "message");
    }

    @Test(expected = NullPointerException.class)
    public void notNullBehavesExactlyLikeTheGuavaMethodItWrapsWithNull() {
        Check.notNull(null, "message");
    }

    @Test
    public void notNullBehavesExactlyLikeTheGuavaMethodItWrapsWithNotNull() {
        Object o = new Object();

        assertSame(o, Check.notNull(o, "message"));
    }
}
