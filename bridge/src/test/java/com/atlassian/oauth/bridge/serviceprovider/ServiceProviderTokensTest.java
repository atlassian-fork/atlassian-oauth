package com.atlassian.oauth.bridge.serviceprovider;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version;
import org.junit.Test;

import static com.atlassian.oauth.bridge.OAuthAccessorBuilder.accessAccessorFor;
import static com.atlassian.oauth.bridge.OAuthAccessorBuilder.authorizedRequestAccessorFor;
import static com.atlassian.oauth.bridge.OAuthAccessorBuilder.unauthorizedRequestAccessorFor;
import static com.atlassian.oauth.testing.Matchers.equalTo;
import static com.atlassian.oauth.testing.TestData.Consumers.HMAC_CONSUMER;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER;
import static com.atlassian.oauth.testing.TestData.OAuthConsumers.HMAC_OAUTH_CONSUMER;
import static com.atlassian.oauth.testing.TestData.OAuthConsumers.RSA_OAUTH_CONSUMER;
import static com.atlassian.oauth.testing.TestData.ServiceProviders.SERVICE_PROVIDER;
import static com.atlassian.oauth.testing.TestData.USER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class ServiceProviderTokensTest {
    @Test
    public void assertThatWeCanConvertUnauthorizedRequestServiceProviderTokensToOAuthAccessorsWithoutTheConsumerPrivateKey() {
        assertThat(
                ServiceProviderTokens.asOAuthAccessor(unauthorizedRequestServiceProviderTokenFor(RSA_CONSUMER), SERVICE_PROVIDER),
                is(equalTo(unauthorizedRequestAccessorFor(RSA_OAUTH_CONSUMER)))
        );
    }

    @Test
    public void assertThatWeCanConvertAuthorizedRequestServiceProviderTokensToOAuthAccessorsWithoutTheConsumerPrivateKey() {
        assertThat(
                ServiceProviderTokens.asOAuthAccessor(authorizedRequestServiceProviderTokenFor(RSA_CONSUMER), SERVICE_PROVIDER),
                is(equalTo(authorizedRequestAccessorFor(RSA_OAUTH_CONSUMER)))
        );
    }

    @Test
    public void assertThatWeCanConvertAccessServiceProviderConsumerTokensToOAuthAccessorsWithoutTheConsumerPrivateKey() {
        assertThat(
                ServiceProviderTokens.asOAuthAccessor(accessServiceProviderTokenFor(RSA_CONSUMER), SERVICE_PROVIDER),
                is(equalTo(accessAccessorFor(RSA_OAUTH_CONSUMER)))
        );
    }

    @Test
    public void assertThatWeCanConvertUnauthorizedRequestServiceProviderTokensToOAuthAccessorsWithoutTheConsumerSecret() {
        assertThat(
                ServiceProviderTokens.asOAuthAccessor(unauthorizedRequestServiceProviderTokenFor(HMAC_CONSUMER), SERVICE_PROVIDER),
                is(equalTo(unauthorizedRequestAccessorFor(HMAC_OAUTH_CONSUMER)))
        );
    }

    @Test
    public void assertThatWeCanConvertAuthorizedRequestServiceProviderTokensToOAuthAccessorsWithoutTheConsumerSecret() {
        assertThat(
                ServiceProviderTokens.asOAuthAccessor(authorizedRequestServiceProviderTokenFor(HMAC_CONSUMER), SERVICE_PROVIDER),
                is(equalTo(authorizedRequestAccessorFor(HMAC_OAUTH_CONSUMER)))
        );
    }

    @Test
    public void assertThatWeCanConvertAccessServiceProviderTokensToOAuthAccessorsWithoutTheConsumerSecret() {
        assertThat(
                ServiceProviderTokens.asOAuthAccessor(accessServiceProviderTokenFor(HMAC_CONSUMER), SERVICE_PROVIDER),
                is(equalTo(accessAccessorFor(HMAC_OAUTH_CONSUMER)))
        );
    }

    private ServiceProviderToken unauthorizedRequestServiceProviderTokenFor(Consumer consumer) {
        return ServiceProviderToken.newRequestToken("1234").tokenSecret("5678").consumer(consumer).creationTime(1234567890L).version(Version.V_1_0_A).build();
    }

    private ServiceProviderToken authorizedRequestServiceProviderTokenFor(Consumer consumer) {
        return unauthorizedRequestServiceProviderTokenFor(consumer).authorize(USER, "9876");
    }

    private ServiceProviderToken accessServiceProviderTokenFor(Consumer consumer) {
        return ServiceProviderToken.newAccessToken("1234").tokenSecret("5678").consumer(consumer).creationTime(1234567890L).authorizedBy(USER).build();
    }
}
