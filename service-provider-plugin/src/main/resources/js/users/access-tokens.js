var AJS = AJS || {};

AJS.$(function() {
    var revokeConfirmation = AJS.$("#revoke-confirmation-text").val(),
        revokeErrorMessage = AJS.$("#revoke-error-message").val(),

        revokeToken = function(token) {
            var consumerName = AJS.$("#token-" + token + "-name").text();
            if (!confirm(AJS.format(revokeConfirmation, consumerName))) {
                return;
            }
            AJS.$.ajax({
                type: "POST",
                data: { token: token },
                success: function() {
                    removeTokenItemFromList(token);
                },
                error: function() {
                    alert(revokeErrorMessage);
                }
            });
        },

        removeTokenItemFromList = function(token) {
            var tokenItem = AJS.$(".revoke[href$=" + token + "]").parents(".token-item");

            var nextDivider = tokenItem.next(".divider");
            var prevDivider = tokenItem.prev(".divider");
            if (nextDivider.length) {
                nextDivider.remove();
            } else if (prevDivider.length) {
                prevDivider.remove();
            }

            tokenItem.remove();
        },

        getConsumerUri = function(iconDiv) {
            var consumerUri = AJS.$(iconDiv).closest(".token-item").find("a.consumerUri");

            if (consumerUri && consumerUri.length) {
                return consumerUri.attr("href");
            }
            return null;
        },

        loadIcon = function(iconDiv, consumerUri) {
            var faviconUri = consumerUri + "/favicon.ico",
                faviconImg = AJS.$('<img class="remote-icon" alt="">');

            faviconImg.load(function() {
                iconDiv.removeClass("default-icon").empty();
                AJS.$(this).appendTo(iconDiv);
            });

            faviconImg.attr("src", faviconUri);
        };

    AJS.$(".revoke").each(function() {
        AJS.$(this).click(function(event) {
            revokeToken(this.href.substring(this.href.lastIndexOf("#") + 1));
            event.preventDefault();
        });
    });

    AJS.$(".app-icon").each(function() {
        var iconDiv = AJS.$(this),
            consumerUri = getConsumerUri(iconDiv);

        if (consumerUri) {
            loadIcon(iconDiv, consumerUri);
        }
    });
});
