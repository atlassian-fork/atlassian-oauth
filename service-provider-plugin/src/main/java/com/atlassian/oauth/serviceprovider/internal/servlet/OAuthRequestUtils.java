package com.atlassian.oauth.serviceprovider.internal.servlet;

import com.google.common.collect.ImmutableSet;
import net.oauth.OAuth;
import net.oauth.server.HttpRequestMessage;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.atlassian.oauth.serviceprovider.internal.util.Paths.ACCESS_TOKEN;
import static com.atlassian.oauth.serviceprovider.internal.util.Paths.REQUEST_TOKEN;
import static net.oauth.OAuth.OAUTH_CONSUMER_KEY;
import static net.oauth.OAuth.OAUTH_NONCE;
import static net.oauth.OAuth.OAUTH_SIGNATURE;
import static net.oauth.OAuth.OAUTH_SIGNATURE_METHOD;
import static net.oauth.OAuth.OAUTH_TIMESTAMP;
import static net.oauth.OAuth.OAUTH_TOKEN;

/**
 * Utility class for extracting information from OAuth requests.
 */
final public class OAuthRequestUtils {
    // Not for instantiation.
    private OAuthRequestUtils() {
    }

    static final Set<String> OAUTH_DATA_REQUEST_PARAMS
            = ImmutableSet.of(OAUTH_CONSUMER_KEY,
            OAUTH_TOKEN,
            OAUTH_SIGNATURE_METHOD,
            OAUTH_SIGNATURE,
            OAUTH_TIMESTAMP,
            OAUTH_NONCE);

    /**
     * Checks if the request is any form of OAuth request, either 2LO or 3LO.
     * It is done by checking the request parameters.
     *
     * @param request the request object.
     * @return true if the request is an OAuth request.
     */
    public static boolean isOAuthAccessAttempt(HttpServletRequest request) {
        return is3LOAuthAccessAttempt(request) || is2LOAuthAccessAttempt(request);
    }

    /**
     * This is 2LO trying to access an OAuth protected resource if all the 2LO parameters are set while 3LO specific
     * parameters are not present.
     *
     * @param request the request object.
     * @return true if the request is an 2LO request.
     */
    public static boolean is2LOAuthAccessAttempt(HttpServletRequest request) {
        final Map<String, String> params = extractParameters(request);

        // http://oauth.googlecode.com/svn/spec/ext/consumer_request/1.0/drafts/2/spec.html
        // oauth_token: MUST be included with an empty value to indicate this is a two-legged request
        return params.keySet().containsAll(OAUTH_DATA_REQUEST_PARAMS) &&
                "".equals(params.get(OAuth.OAUTH_TOKEN)) &&
                !isRequestTokenRequest(request);
    }

    /**
     * This is 3LO trying to access an OAuth protected resource if all the 3LO parameters are set and we aren't trying to
     * turn a request token into an access token (which is the only other time all the OAuth parameters are in the
     * request).
     *
     * @param request the request object.
     * @return true if the request is an 3LO request.
     */
    public static boolean is3LOAuthAccessAttempt(HttpServletRequest request) {
        final Map<String, String> params = extractParameters(request);

        // all the oauth request parameters must be present and oauth_token must not be empty
        return params.keySet().containsAll(OAUTH_DATA_REQUEST_PARAMS) &&
                params.containsKey(OAuth.OAUTH_TOKEN) &&
                !"".equals(params.get(OAuth.OAUTH_TOKEN)) &&
                !isAccessTokenRequest(request);
    }

    /**
     * Checks if this request is a request token request.
     *
     * @param request the request object.
     * @return true if it's a request token request.
     */
    private static boolean isRequestTokenRequest(HttpServletRequest request) {
        return request.getRequestURI().endsWith(REQUEST_TOKEN);
    }

    /**
     * Checks if this request is an access token request.
     *
     * @param request the request object.
     * @return true if it's an access token request.
     */
    private static boolean isAccessTokenRequest(HttpServletRequest request) {
        return request.getRequestURI().endsWith(ACCESS_TOKEN);
    }

    /**
     * @return available parameters in the request.
     */
    private static Map<String, String> extractParameters(HttpServletRequest request) {
        Map<String, String> params = new HashMap<String, String>();
        for (OAuth.Parameter param : HttpRequestMessage.getParameters(request)) {
            params.put(param.getKey(), param.getValue());
        }
        return Collections.unmodifiableMap(params);
    }
}