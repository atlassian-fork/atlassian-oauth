package com.atlassian.oauth.consumer;

/**
 * Exception thrown if a consumer cannot be found.
 */
public class OAuthConsumerNotFoundException extends OAuthConsumerException {
    public OAuthConsumerNotFoundException(String message) {
        super(message);
    }
}
